package com.vrmdevops.junit.helper;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringHelperTest {

	@Test
	public void testTruncateAInFirst2Positions() {
		StringHelper helper = new StringHelper();
		String v_expected = helper.truncateAInFirst2Positions("AABCD");
		String v_actual = helper.truncateAInFirst2Positions("BCD"); 
		assertEquals(v_expected, v_actual);
	}

}
