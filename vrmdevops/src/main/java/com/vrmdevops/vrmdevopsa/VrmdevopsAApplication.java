package com.vrmdevops.vrmdevopsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VrmdevopsAApplication {

	public static void main(String[] args) {
		SpringApplication.run(VrmdevopsAApplication.class, args);
	}

}
